<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Inventory;

class InventoryList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inventory';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List inventory stock';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $objs = Inventory::nonEmpty()->get();
        // no stock? can exit here
        if(count($objs) < 1){
            $this->info('Your inventory empty');
            return ;
        }

        // print stock
        foreach($objs as $obj){
            $this->info(
                sprintf(
                    'QTY: %u Cost: %01.2f Created at: %s', 
                    $obj->qty, $obj->cost, $obj->created_at->format('Y-m-d H:i:s')
                )
            );
        }
    }
}