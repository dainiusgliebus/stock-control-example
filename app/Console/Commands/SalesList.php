<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Sale;

class SalesList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sales';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List sales';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $objs = Sale::get();
        // if empty can finish
        if(count($objs) < 1){
            $this->info('No sales found.');
            return ;
        }

        // print stock
        foreach($objs as $obj){
            $this->info(
                sprintf(
                    'QTY: %u Cost: %01.2f Price: %01.2f Created At: %s', 
                    $obj->qty, $obj->cost, $obj->price, $obj->created_at->format('Y-m-d H:i:s')
                )
            );
        }
    }
}