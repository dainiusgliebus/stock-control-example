<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Sale;
use App\Models\Inventory;

class Sell extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sell {qty} {price}';

    /**
     * The console command description.
     *
     * @var string 
     */
    protected $description = 'Add sale with price';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $required = (int) $this->argument('qty');
        if($required < 1){
            $this->error('Bad sale qty passed.');
            return ;
        }
        
        $price = round((float) $this->argument('price'), 2);
        if($price < 0){
            $this->error('Bad sale price passed.');
            return ;
        }
        
        $this->info(sprintf('Sold qty %u with price of %01.2f.', $required, $price));

        // support only one product so dont need to make it too complicated
        $stock = Inventory::nonEmpty()->get();

        // check if we have any stock
        if(count($stock) < 1){
            $this->error('No stock.');
            return ;
        }

        foreach($stock as $obj){
            // all good can exit 
            if($required < 1){
                break;
            }

            // inventory has more than required! super!
            if($obj->qty >= $required){
                $this->storeSale($obj->id, $obj->cost, $price, $required);
                $this->deductStock($obj, $required);
                $required = 0;
            }else if($obj->qty < $required){
                $this->storeSale($obj->id, $obj->cost, $price, $obj->qty);
                $required = $obj->qty;
                $this->deductStock($obj, $obj->qty);
            }
        }

        if($required > 0){
            $this->error(sprintf('Magic! Sold %u units more than had in inventory!', $required));
        } else {
            $this->info('Sales data stored.');
        }
        
    }

    /** 
     * Deduct stock from inventory
     * @param Iventory $obj inventory object
     * @param int $qty deduct stock number
     * @return void
    */
    private function deductStock(Inventory $obj, int $qty){
        $obj->qty -= $qty;
        $obj->save();
    }

    /** 
     * Store sales info
     * More practical option to store 1 item per line easier to manage on long term
     * can track if item was returned as well batch number (can be different for same item)
     * @param int $inv inventory id
     * @param float $cost cost of product
     * @param float $price sale price
     * @param int $times number of times to store identical sold item info
     * @return void
    */
    private function storeSale(int $inv, float $cost, float $price, int $times){
        for($i = 0; $i < $times; $i++){
            $sale = new Sale;
            $sale->qty = 1;
            $sale->inventory_id = $inv;
            $sale->cost = $cost;
            $sale->price = $price;
            $sale->save();
        }
    }
}