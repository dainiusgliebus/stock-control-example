<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Inventory;

class Buy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'buy {qty} {cost}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add products to inventory';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $qty = (int) $this->argument('qty');
        $qty = ($qty > 0) ? $qty : 1; // default to 1
        $cost = round((float) $this->argument('cost'), 2);
        
        if($cost < 0){
            $this->error('Incorrect price passed.');
            return ;
        }

        $this->info(sprintf('Adding qty %u with cost of %01.2f.', $qty, $cost));

        // ok, lets add stock
        $inv = new Inventory;
        $inv->start_qty = $qty;
        $inv->qty = $qty;
        $inv->cost = $cost;
        
        if($inv->save()){
            $this->info('Stored new inventory');
        } else {
            $this->error('Failed to store new inventory');
        }
    }
}