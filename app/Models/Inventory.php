<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    /**
     * Scope a query to only include non empty stock.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNonEmpty($query)
    {
        return $query->where('qty', '>', 0);
    }

    protected $table = 'inventory';
}
