## Setup

```sh
# clone repot from git
$ git clone git@bitbucket.org:dainiusgliebus/stock-control-example.git
# install components with composer
$ composer install
# copy example .env.example to .env and enter mysql database details
$ cp .env.example .env
# setup database command
$ php artisan migrate
```

## Commands

```sh
# list current stock info
$ php artisan inventory
# list sales info
$ php artisan sales
# calculate margin
$ php artisan margin
# add stock to invetory
$ php artisan buy {qty} {cost}
# add sale
$ php artisan sell {qty} {price}
```